const path = require("path"),
    webpack = require("webpack"),
    CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: "./src/js/main.jsx",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist/js"),
        publicPath: path.resolve(__dirname, "dist/"),
        libraryTarget: "var",
        library: "Foire"
    },
    devtool: 'source-map',
    plugins: [
        new webpack.EnvironmentPlugin(['API_URL', 'SITE_URL', 'FACEBOOK_APP_ID']),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, "src/images/") ,
            to: path.resolve(__dirname, "dist/images/") 
        }])
    ],
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            snapsvg: 'snapsvg/dist/snap.svg.js',
        },
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|eot|ttf|woff|woff2)$/,
                use: {
                    loader: "url-loader",
                    options: {
                        limit: 10000,
                        name: '../images/[name].[hash:7].[ext]'
                    }
                }
            },
            {
                test: /\.svg$/,
                use: {
                    loader: 'svg-url-loader',
                    options: {
                        limit: 100000
                    }
                }
            },
            {
                test: /\.(png|jpg|gif|mp4)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        publicPath: 'files/',
                        outputPath: '../files/'
                    }
                }
            },
            {
                test: /\.(css|scss|sass)$/,
                use: [
                    { loader: "style-loader" },
                    {
                        loader: "css-loader",
                        options: {
                            use: false
                        }
                    },
                    { loader: "sass-loader" }
                ]
            },
            {
                test: /\.jsx?$/,
                exclude:  /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ["es2015", "react"]
                    }
                },
            },
            {
                test: require.resolve('snapsvg/dist/snap.svg.js'),
                use: 'imports-loader?this=>window,fix=>module.exports=0',
            },
        ]
    }
};
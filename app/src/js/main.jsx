"use strict";

import emojify from "emojify.js";
import React from "react";
import {render} from "react-dom";
import App from "./app.jsx";

import css from "../sass/index.sass";

emojify.setConfig({
    mode : "img",
    img_dir: "images/emojify.js"
});

render(
    <App />,
    document.getElementById("main")
);
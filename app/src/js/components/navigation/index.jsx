import React from "react";

export default class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.navigationElement = null;
    }

    render () {
        return (
            <nav
                ref={ref => this.navigationElement = ref}
                className="navigation game__navigation">
                <span
                    className="navigation__control button"
                    tabIndex="0"
                    onClick={() => this.props.onNextItem()}
                >
                    <span className="button__inner">
                        Click or <span>Press enter</span>
                    </span>
                </span>
            </nav>
        );
    }
};

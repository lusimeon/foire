import React from "react";
import * as ReactRedux from "react-redux";
import LeaderBoard from "./leaderboard/index.jsx";
import Add from "./leaderboard/add.jsx";
import SocialShare from "./socialshare/index.jsx";
import Score from "./score/index.jsx";
import Border from './misc/border.jsx';
import * as actions from "../actions/leaderboard";
import { Redirect } from "react-router";

class Result extends React.Component {
    render () {
        let content = ', essai de faire mieux pour pouvoir l\'ajouter au tableau des scores.';

        if (this.props.score > 0) {
            content = ', tu peux l\'ajouter au tableau si tu le souhaites.';
            
            if (this.props.isSubmitted === true) {
                content = ', merci pour ta participation.';
            }
        }       
        return (
            <article className="result">
                <Border
                    launch={this.props.isEntered}
                    gradientColor='l(1, 1, 0, 0)#5B86E5-#36D1DC:120'
                    logo={true}
                    logoAnimation={false}
                />
                <div>
                    <div>
                        <div className="result__leaderboard_add">
                            <div className='text text--highlight'>
                                <span></span>
                                <span>{`Ton score est de ${this.props.score}${content}`}</span>
                            </div>
                            {this.props.isSubmitted === false && this.props.score > 0 &&
                                <Add
                                    onSubmit={this.props.addScore.bind(null, this.props.score)}
                                />
                            }
                        </div>
                        <div className="result__socialshare">
                            <div className='text text--highlight'>
                                <span></span>
                                <span>Tu peux partager ce site sur les réseaux sociaux, ça fait toujours plaisir.</span>
                            </div>
                            <SocialShare
                                score={this.props.score}
                            />
                        </div>
                    </div>
                    <LeaderBoard
                        leaderboard={this.props.leaderboard}
                        onLoad={this.props.loadLeaderboard}
                    />
                </div>
            </article>
        );
    }
};

export default ReactRedux.connect(
    (state = {}) => state,
    (dispatch, props) => Object.assign({}, props, {
        "loadLeaderboard": actions.load.bind(null, dispatch, process.env.API_URL + "/leaderboard/"),
        "addScore": actions.add.bind(null, dispatch, process.env.API_URL + "/leaderboard/")
    })
)(Result);
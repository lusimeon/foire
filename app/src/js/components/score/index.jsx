import React from "react";

export default class Score extends React.Component {
    render () {
        return (
            <span className="game__score score">{`🌟 ${this.props.score}`}</span>
        );
    }
};
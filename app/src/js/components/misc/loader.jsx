import React from "react";

export default class Loader extends React.Component {
    render () {
        return (
            <span className="loader score">{`🌟 ${this.props.score}`}</span>
        );
    }
};
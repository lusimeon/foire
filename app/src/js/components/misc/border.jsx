import Snap from 'snapsvg';
import React from "react";
import {TweenLite, TimelineLite, TimelineMax} from "gsap";

const timeRange = 300;

const randomMvmt = function(timelines, elements, x, y, duration, ease) {
    const randomX = x || Math.floor(Math.random() * this.state.originalSize.w) + 1;
    const randomY = y || Math.floor(Math.random() * this.state.originalSize.h) + 1;
    const time = duration || Math.floor(Math.random() * 3) + 2;
    const easing = ease || SlowMo.ease.config(0.5, 0.7, false);

    for (var i = 0; i < timelines.length; i++) {
        const timeline = timelines[i];
        const element = elements[i];
        
        let options = {
            ease: easing,
            attr: {
                cx: randomX,
                cy: randomY,
                x: randomX,
                y: randomY
            }
        };

        if (i === timelines.length - 1) {
            options['onComplete'] = randomMvmt.bind(this, timelines, elements);
        }

        timeline
            .set(element, {
                opacity: 1
            })
            .to(element, time, options)
        ;
    }
};

export default class Border extends React.Component {
    constructor(props) {
        super(props);

        this.logoLaunched = false;
        this.containerElement = this.svgElement = this.shadowElement = null;
        this.svgFillElement = this.svgBorderElement = this.svgBorderContainerElement = null;
        
        this.svgCircleElement = this.svgRectElement = this.svgRect2Element = this.svgRectElementClone = null;
        this.svgCircle2Element = this.svgCircle2ElementClone = this.svgTextElement = this.svgTextElementClone = null;
        
        this.svgBorderView = this.svgGradient = this.svgFillPath = this.svgBorderPath = this.svgRectPath = this.svgTextPath = this.svgCircle2Path = null;

        this.svgBorderElementAnimation = null;
        this.svgFillElementAnimation = null;

        this.svgElementTimeline = new TimelineLite({paused: true});        
        this.svgTextElementTimeline = new TimelineMax();
        
        this.svgRectElementTimeline = new TimelineMax();
        this.svgRectElementCloneTimeline = new TimelineMax();
        this.svgRect2ElementTimeline = new TimelineMax();
        this.svgCircleElementTimeline = new TimelineMax();
        this.svgCircle2ElementTimeline = new TimelineMax();
        this.svgCircle2ElementCloneTimeline = new TimelineMax();

        // Hack to prevent undefined on first display
        this.defaultSize = {
            w: 750,
            h: 300    
        };

        this.state = {
            originalSize: this.defaultSize,
            svgSize: this.defaultSize,
            steps: [] // Differents paths to morph
        };
    }

    componentDidMount() {
        // Add border svg
        this.svgBorderView = Snap(this.svgElement);
        this.svgFillPath = Snap(this.svgFillElement);
        this.svgBorderPath = Snap(this.svgBorderElement);

        if (this.props.gradientColor !== null) {
            this.svgGradient = this.svgBorderView.gradient(this.props.gradientColor);   

            this.svgFillPath.attr({
                fill: this.svgGradient
            });
        }

        // Update paths on resize event 
        this.updateDimensions();
        window.addEventListener('resize', () => this.updateDimensions());

        // Create timelines
        this.svgElementTimeline
            .to([this.svgElement, this.containerElement.nextSibling], 1, {
                padding: '3rem',
                onUpdate: this.updateDimensions.bind(this),
                onComplete: () => {
                    randomMvmt.call(this, [this.svgCircleElementTimeline], [this.svgCircleElement]);
                    randomMvmt.call(this, [this.svgCircle2ElementTimeline, this.svgCircle2ElementCloneTimeline], [this.svgCircle2Element, this.svgCircle2ElementClone]);
                    randomMvmt.call(this, [this.svgRectElementTimeline, this.svgRectElementCloneTimeline], [this.svgRectElement, this.svgRectElementClone]);
                    randomMvmt.call(this, [this.svgRect2ElementTimeline], [this.svgRect2Element]);    

                    this.svgBorderElementAnimation = this.animate(this.svgBorderPath, null);
                    this.svgFillElementAnimation = this.animate(this.svgFillPath, null);
                }
            }, "start")
            .to(this.shadowElement, 1, {
                top: '3.75rem',
                left: '3.75rem',
                right: '3.75rem',
                bottom: '3.75rem'
            }, "start")
        ;

        // Launch letters animation
        this.svgTextElementTimeline
            .to([this.svgTextElement, this.svgTextElementClone], 4, {
                ease: Power1.easeIn,
                delay: 0.4,
                repeat: -1,
                yoyo: true,
                attr: {
                    dx: '2,-2,-2,0,2',
                    dy: '2,8,-5,10,-13'
                }
            })
        ;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.launch === false && this.props.launch === true) {
            if (this.props.logoAnimation === true) {
                TweenLite
                    .to([this.svgTextElement, this.svgTextElementClone], this.props.logoAnimation ? 1.5 : 0, {
                        ease: Power2.easeInOut,
                        attr: {
                            'font-size': '5em',
                            x: '10%',
                            y: '3%'
                        },
                        onStart: () => {
                            this.svgTextElementTimeline.pause();        
                        } 
                    })
                ;
            }

            this.svgElementTimeline.play();        
        } else if (prevProps.launch === true && this.props.launch === false) {
            // Stop snap.svg animations
            if (this.svgBorderElementAnimation !== null) {
                this.svgBorderElementAnimation.stop();
            }

            if (this.svgFillElementAnimation !== null) {
                this.svgFillElementAnimation.stop();
            }

            // Exclude objects from the view (dirty hack)
            // Exclude circle
            this.svgCircleElementTimeline
                .kill(null, this.svgCircleElement)
                .to(this.svgCircleElement, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        cx: prevState.originalSize.w * 1.2,
                        cy: prevState.originalSize.h * 0.3,
                    }
            });

            // Exclude rect2
            this.svgRect2ElementTimeline
                .kill(null, this.svgRect2Element)
                .to(this.svgRect2Element, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        x: prevState.originalSize.w * 1.2,
                        y: prevState.originalSize.h * 0.3,
                    }
            });

            // Exclude rect
            this.svgRectElementTimeline
                .kill(null, this.svgRectElement)
                .to(this.svgRectElement, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        x: prevState.originalSize.w * -0.5,
                        y: prevState.originalSize.h * 0.6,
                    }
            });

            this.svgRectElementCloneTimeline
                .kill(null, this.svgRectElementClone)
                .to(this.svgRectElementClone, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        x: prevState.originalSize.w * -0.5,
                        y: prevState.originalSize.h * 0.6,
                    }
            });

            // Exclude Circle2
            this.svgCircle2ElementTimeline
                .kill(null, this.svgCircle2Element)
                .to(this.svgCircle2Element, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        cx: prevState.originalSize.w * -0.5,
                        cy: prevState.originalSize.h * 0.6,
                    }
            });

            this.svgCircle2ElementCloneTimeline
                .kill(null, this.svgCircle2ElementClone)
                .to(this.svgCircle2ElementClone, 1, {
                    ease: Power4.easeIn,
                    opacity: 0,
                    attr: {
                        cx: prevState.originalSize.w * -0.5,
                        cy: prevState.originalSize.h* 0.6,
                    }
                })
                .add(() => {
                    // +++ Be warn other object animation before remove this part +++
                    // Launch svg and text animation when objects leave scene
                    if (this.props.logoAnimation === true) {
                        TweenLite.to([this.svgTextElement, this.svgTextElementClone], 1.5, {
                            ease: Power2.easeInOut,
                            attr: {
                                'font-size': '10em',
                                x: '50%',
                                y: '33%',
                                dx: '0,0,10,0,7',
                                dy: '0,15,-10,20,-28'
                            },
                            onComplete: () => {
                                this.svgTextElementTimeline.play();
                            }
                        });
                    } 
                
                    this.svgElementTimeline.reverse();
                }, "-=1")
            ;


        }

        if (prevProps.gradientColor !== this.props.gradientColor) {
            this.svgGradient.remove();
            this.svgGradient = this.svgBorderView.gradient(this.props.gradientColor);   
            
            this.svgFillPath.attr({
                fill: this.svgGradient
            });
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', () => this.updateDimensions())
    }

    updateDimensions() {
        if (this.svgElement !== null) {
            const computedStyle = getComputedStyle(this.svgElement);

            let elementHeight = this.svgElement.clientHeight || parseInt(computedStyle.height, 10);
            let elementWidth = this.svgElement.clientWidth || parseInt(computedStyle.width, 10);

            const originalSize = {
                w: elementWidth,
                h: elementHeight
            };

            const size = {
                w: elementWidth - (parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight)),
                h: elementHeight - (parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom))
            };

            const steps = [
                `M-3,-1 L 2,${size.h - (3 * 3)} ${size.w + (2 * 3)},${size.h + (2 * 3)} ${size.w - (3 * 3)},2 -3,-1`,
                `M3,2 L -3,${size.h - (3 * 3)} ${size.w - (3 * 3)},${size.h + (3 * 3)} ${size.w + (3 * 3)},-2 3,2`,
                `M3,-2 L 3,${size.h + (3 * 3)} ${size.w + (3 * 3)},${size.h - (3 * 3)} ${size.w - (3 * 3)},3 3,-2`
            ];

            this.setState({
                originalSize: originalSize,
                svgSize: size,
                steps: steps
            });
        }
    };

    animate(element, lastStepNumber = null) {
        const randomNumber = Math.floor(Math.random() * this.state.steps.length) + 1;
        
        // Ensure that random number is not same as last random number
        if (randomNumber === lastStepNumber) {
            return this.animate(element, lastStepNumber);
        }

        const animation = element.animate({
            d: this.state.steps[randomNumber - 1],
        }, Math.floor(Math.random() * timeRange) + timeRange, mina.easein(), this.animate.bind(this, element, randomNumber));

        return animation;
    };

    onMouseEnter() {
        if (this.props.onMouseEnter !== undefined) {
            this.props.onMouseEnter();
        }
    }

    onMouseLeave() {
        if (this.props.onMouseLeave !== undefined) {
            this.props.onMouseLeave();
        }
    }

    render () {
        return (
            <div
                ref={ref => this.containerElement = ref}
                className='border_container'
            >
                <svg
                    ref={ref => this.svgElement = ref}
                    className='border_container__svg'
                >
                    <defs>
                        <clipPath id="border_container__mask">
                            <use href="#svgBorder__fill" x="0" y="0" width={this.state.originalSize.w}/>
                        </clipPath>
                    </defs>

                    { this.props.logo &&
                        <text
                            ref={ref => this.svgTextElementClone = ref}
                            className="border_container__object"
                            fontFamily="Wireframe"
                            stroke="none"
                            fontSize={this.props.logoAnimation ? "10em" : "5em"}
                            fill="black"
                            x={this.props.logoAnimation ? "50%" : "10%"}
                            y={this.props.logoAnimation ? "33%" : "3%"}
                            dx="0,0,10,0,7"
                            dy="0,15,-10,20,-28"
                            textAnchor="middle"
                        >
                            <title>Foire</title>
                            Foire
                        </text>
                    }
                    <rect
                        className="border_container__object border_container__object--animation"
                        ref={ref => this.svgRectElementClone = ref}
                        x={this.state.originalSize.w * -0.4}
                        y={this.state.originalSize.h * 0.4}
                        width="300"
                        height="200"
                        fill="none"
                        stroke="black"
                        strokeWidth="6"
                    />
                    <circle
                        ref={ref => this.svgCircle2ElementClone = ref}
                        className="border_container__object border_container__object--animation"
                        cx={this.state.originalSize.w * 1.5}
                        cy={this.state.originalSize.w * 0.2}
                        r="60"
                        fill="none"
                        stroke="black"
                        strokeWidth="4"
                    />
                    <g
                        ref={ref => this.svgBorderContainerElement = ref}
                    >
                        <path
                            id="svgBorder__fill"
                            ref={ref => this.svgFillElement = ref}
                            className='svgBorder__fill'
                            d={`M0,0 L 0,${this.state.svgSize.h} ${this.state.svgSize.w},${this.state.svgSize.h} ${this.state.svgSize.w},0 0,0`}
                            onMouseEnter={this.onMouseEnter.bind(this)}
                            onMouseLeave={this.onMouseLeave.bind(this)}
                        ></path>
                        <path
                            ref={ref => this.svgBorderElement = ref}
                            className='svgBorder__border border_container__object'
                            d={`M0,0 L 0,${this.state.svgSize.h} ${this.state.svgSize.w},${this.state.svgSize.h} ${this.state.svgSize.w},0 0,0`}
                            fill='none'
                            stroke='black'
                            strokeWidth='2'
                            vectorEffect='non-scaling-stroke'
                        ></path>
                    </g>
                    { this.props.logo &&
                        <text
                            ref={ref => this.svgTextElement = ref}
                            className="border_container__object"
                            clipPath="url(#border_container__mask)"
                            fontFamily="Wireframe"
                            fill="white"
                            stroke="none"
                            fontSize={this.props.logoAnimation ? "10em" : "5em"}
                            x={this.props.logoAnimation ? "50%" : "10%"}
                            y={this.props.logoAnimation ? "33%" : "3%"}
                            dx="0,0,10,0,7"
                            dy="0,15,-10,20,-28"
                            textAnchor="middle"
                        >
                            <title>Foire</title>
                            Foire
                        </text>
                        
                    }
                    <rect
                        className="border_container__object border_container__object--animation"
                        ref={ref => this.svgRectElement = ref}
                        x={this.state.originalSize.h * -0.4}
                        y={this.state.originalSize.h * 0.4}
                        width="300"
                        height="200"
                        fill="none"
                        stroke="white"
                        strokeWidth="6"
                        clipPath="url(#border_container__mask)"
                    />
                    <circle
                        ref={ref => this.svgCircleElement = ref}
                        className="border_container__object border_container__object--blend border_container__object--animation"
                        cx={this.state.originalSize.h * 1.5}
                        cy={this.state.originalSize.h * 0.2}
                        r="100"
                        fill="none"
                        stroke="black"
                        strokeWidth="4"
                    />
                    <rect
                        className="border_container__object border_container__object--blend border_container__object--animation"
                        ref={ref => this.svgRect2Element = ref}
                        style={{
                            transform: 'skew(10deg)'
                        }}
                        x={this.state.originalSize.w * -0.5}
                        y={this.state.originalSize.h * 0.9}
                        width="100"
                        height="120"
                        fill="none"
                        stroke="blue"
                        strokeWidth="6"
                    />
                    <circle
                        clipPath="url(#border_container__mask)"
                        ref={ref => this.svgCircle2Element = ref}
                        className="border_container__object border_container__object--animation"
                        cx={this.state.originalSize.w * 1.5}
                        cy={this.state.originalSize.h * 0.2}
                        r="60"
                        fill="none"
                        stroke="white"
                        strokeWidth="4"
                    />
                </svg>
                <div
                    ref={ref => this.shadowElement = ref}
                    className='border_container__shadow'
                ></div>
            </div>
        );
    }
};
"use strict";

import React from "react";
import * as ReactRedux from "react-redux";
import Item from "./item/index.jsx";
import Navigation from "./navigation/index.jsx";
import Score from "./score/index.jsx";
import Timer from "./timer/index.jsx";
import Loader from "./misc/loader.jsx";
import * as itemActions from "../actions/item.js";
import * as timerActions from "../actions/timer.js";
import * as navigationActions from "../actions/navigation.js";
import * as scoreActions from "../actions/score.js";
import { Redirect } from "react-router";
import Border from './misc/border.jsx';
import {TweenLite, TimelineLite} from 'gsap';

class Items extends React.Component {
    constructor(props) {
        super(props);

        this.props.load();

        this.borderAnimation = false;
        this.navigationAnimation = false;

        this.navigationMvmtTimeline = new TimelineLite();

        this.borderedElement = null;
        this.navigationElement = null;
    }

    componentWillMount() {
        this.props.setCurrentItem(0);
        this.props.setScore(0);
    }

    componentDidUpdate() {
        if (this.borderAnimation === false && this.borderedElement !== null) {
            this.borderAnimation = true;

            TweenLite.set(this.borderedElement.svgBorderElement, {
                x: "2%",
                y: "2%",
                force3D: true
            });
        }

        if (this.navigationAnimation === false && this.navigationElement !== null) {
            this.navigationMvmtTimeline.to(this.navigationElement.navigationElement, 1, {
                right: '6rem',
                bottom: '4.5rem'
            });
        }
    }

    componentWillUnmount() {
        this.props.setStage('idle');
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.stage !== 'gameFinished' &&
            this.props.items.length > 0 &&
            nextProps.navigation > this.props.items.length - 1
        ) {
            this.props.setStage('gameFinished');
        }
    }

    render () {
        if (this.props.items.length === 0) {
            return <Loader />;
        }

        const item = this.props.items[this.props.navigation];
        const isInGame = ['gameTimer', 'endCountdown'].includes(this.props.stage);
        
        let gradientColor = 'l(1, 1, 0, 0)#eee-#fff:120';

        if (isInGame === true) {
            if (this.props.navigation % 2 === 0) {
                gradientColor = 'l(1, 0, 0, 0)#fafafa:50-#a2ded0:50';
            } else {
                gradientColor = 'l(0, 0, 1, 0)#fafafa:50-#dcc6e0:50';
            }
        }

        return (
            <article className="game">
                { this.props.stage === 'gameFinished' &&
                    <Redirect to="/result"/>
                }
                <Border
                    ref={ref => this.borderedElement = ref}
                    gradientColor={gradientColor}
                    launch={isInGame}
                    logo={isInGame}
                />
                <div>

                    { isInGame === true && item !== undefined &&
                        <Item
                            item={item}
                            success={this.props.success}
                            response={this.props.response}
                            index={this.props.navigation}
                            onValidate={this.props.validate.bind(null, item._id, this.props.response)}
                        />
                    }
                    
                    { isInGame &&
                        <Navigation
                            ref={ref => this.navigationElement = ref}
                            onNextItem={this.props.validate.bind(null, item._id, this.props.response)}
                        />
                    }
                    
                    <Timer launch={this.props.isEntered}/>
                </div>

            </article>
        );
    }
};

export default ReactRedux.connect(
    (state = {}) => state,
    (dispatch, props) => Object.assign({}, props, {
        "validate": itemActions.validate.bind(null, dispatch, props.validateSource),
        "load": itemActions.load.bind(null, dispatch, props.source),
        "setStage": timerActions.setStage.bind(null, dispatch),
        "setCurrentItem": navigationActions.setCurrentItem.bind(null, dispatch),
        "setScore": scoreActions.setScore.bind(null, dispatch)
    })
)(Items);
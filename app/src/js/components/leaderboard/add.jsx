import React from "react";

export default class Add extends React.Component {
    handleSubmit(event) {
        event.preventDefault();

        const textInput = event.target.querySelector("input[type='text']");

        this.props.onSubmit(textInput.value);
    }

    render () {
        return (
            <form onSubmit={this.handleSubmit.bind(this)} className="form form--inline leaderboard__add">
                <input className="form__control form__control--text" type="text" placeholder="Pseudo"/>
                <button className="button" type="submit">
                    <span className="button__inner">Ajouter</span>
                </button>
            </form>
        );
    }
};
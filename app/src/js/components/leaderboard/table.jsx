import React from "react";

export default class Table extends React.Component {
    constructor(props) {
        super(props);

        this.props.onLoad();
    }

    render () {
        return (
            <div className="leaderboard__table table">
                <div className="table__head">
                    <span>Blaze</span>
                    <span>Score</span>
                </div>
                <div className="table__table">
                    <table>
                        <tbody>
                        { this.props.leaderboard.length === 0 &&
                            <tr>
                                <td>Fait le premier pas !</td>
                            </tr>
                        }
                        { this.props.leaderboard.map((item, index) => {
                            return <tr key={index} >
                                <td>{ item.username }</td>
                                <td>{ item.score }</td>
                            </tr>
                        }) }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
};
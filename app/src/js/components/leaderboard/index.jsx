import React from "react";
import Table from "./table.jsx";

export default class LeaderBoard extends React.Component {
    render () {
        return (
            <div className="leaderboard">
                <Table
                    onLoad={this.props.onLoad}
                    leaderboard={this.props.leaderboard}
                />
            </div>
        );
    }
};
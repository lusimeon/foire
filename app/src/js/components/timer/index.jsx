import React from "react";
import * as ReactRedux from "react-redux";
import * as actions from "../../actions/timer.js"
import {TweenMax} from "gsap";

function createInterval (timeLeft, interval = 1000, callback, onEndCallback) {
    return setInterval(() => {
        timeLeft = timeLeft - interval;

        if (callback !== undefined && callback !== null) {
            callback(timeLeft);
        }

        if (timeLeft <= 0) {
            if (onEndCallback !== undefined && onEndCallback !== null) {
                onEndCallback();
            }
        }
    }, interval);
}


class Timer extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            time: 60000,
            delay: 3000
        }

        this.interval = null;
        this.backgroundElement = null;

        this.defaultDelay = this.state.delay;
        this.defaultTime = this.state.time;
    }
    
    componentWillReceiveProps(nextProps) {
        if (this.props.stage === 'idle' && nextProps.launch === true) {
            this.props.setStage('startCountdown');

            // Launch start countdown
            this.create(this.state.delay, (startCountdown) => {
                // Set state only every 1000 milliseconds to reduce setState calls
                if (startCountdown % 1000 === 0) {
                    this.setState({
                        delay: startCountdown
                    }); 
                }
            }, () => {
                this.props.setStage('gameTimer');

                this.create(this.state.time, (time) => {
                    if (time % 1000 === 0) {
                        this.setState({
                            time: time
                        });
                    }

                    // When time === endCountdown delay launch countdown
                    if (time <= this.defaultDelay) {
                        // Display end countdown
                        this.props.setStage('endCountdown');

                        if (time % 1000 === 0) {
                            this.setState({
                                delay: time
                            });
                        }
                    }
                }, () => this.props.setStage('gameFinished'));
                
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.stage !== this.props.stage && this.props.stage === 'gameTimer') {
            TweenMax.to(this.backgroundElement, this.defaultTime / 1000, {
                ease: Power0.easeNone,
                width: '100%'
            });
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    create(timeLeft, callback, onEndCallback) {
        clearInterval(this.interval);

        this.interval = createInterval(timeLeft, 1000, callback, () => {
            clearInterval(this.interval);
            
            if (onEndCallback !== undefined && onEndCallback !== null) {
                onEndCallback();
            }
        });
    }


    render () {
        if (this.props.stage === 'gameFinished') {
            return null;
        }

        return <div className={`timer timer--${this.props.stage}`}>
            { ['startCountdown', 'endCountdown'].includes(this.props.stage) &&
                <span className="timer__counter game__delay">
                    {`${parseInt(this.state.delay) / 1000}`}
                </span>
            }
            { ['gameTimer', 'endCountdown'].includes(this.props.stage) &&
                <div
                    ref={ref => this.backgroundElement = ref}
                    className="timer__background"
                ></div>
            }
        </div>;
    }
};

export default ReactRedux.connect(
    (state = {}) => state,
    (dispatch, props) => Object.assign({}, props, {
        "setStage": actions.setStage.bind(null, dispatch)
    })
)(Timer);
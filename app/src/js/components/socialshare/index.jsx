import React from "react";

export default class SocialShare extends React.Component {
    constructor(props) {
    	super(props);

    	this.siteUrl = process.env.SITE_URL !== undefined ? encodeURIComponent(process.env.SITE_URL) : null;    	
    	this.sharedText = process.env.SITE_URL !== undefined && this.props.score !== undefined ? encodeURIComponent(`J'ai fais un score de ${this.props.score} sur ${process.env.SITE_URL}`) : null;    	
    }

    render () {

    	if (this.sharedText === null && process.env.FACEBOOK_APP_ID === undefined) {
    		return null;
    	}

        return (
            <div className="socialshare">
            	{this.sharedText !== null &&
					<a
						className="socialshare__item socialshare__item--twitter button button--inline"
						href={`https://twitter.com/intent/tweet?text=${this.sharedText}`}
					>
						<span className='button__inner'>Twitter</span>
					</a>
            	}
            	{process.env.FACEBOOK_APP_ID !== undefined &&
					<a
						className="socialshare__item socialshare__item--facebook button button--inline"
						href={`https://www.facebook.com/sharer/sharer.php?app_id=${process.env.FACEBOOK_APP_ID}&kid_directed_site=0&sdk=joey&u=${this.siteUrl}&display=popup&ref=plugin&src=share_button`}
					>
						<span className='button__inner'>Facebook</span>
					</a>
            	}
            </div>
        );
    }
};
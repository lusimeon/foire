import React from "react";
import { Redirect } from "react-router-dom";
import emojify from "emojify.js";
import Border from './misc/border.jsx';
import { withRouter } from 'react-router-dom';
import parallax from '../utilities/parallax.js';
import {TweenLite} from 'gsap';

class Home extends React.Component {
    constructor(props) {
        super(props);

        this.rulesTimeline = new TimelineLite({paused: true});

        this.rulesElement = null;
        this.logoElement = null;
        this.buttonElement = null;
        this.borderElement = null;

        this.state = {
            launchAnimation: false,
            launchButtonParralax: false
        };
    }

    componentDidMount() {
        const mainElement = document.querySelector("#main");
        const rulesElements = this.rulesElement.querySelector(":not(:last-child)");

        this.rulesTimeline.to(rulesElements, 1.2, {
            ease: Power2.easeInOut,
            delay: 0.3,
            marginBottom: '0.75em'
        });

        TweenLite.set(this.borderElement.svgBorderElement, {
            x: "2%",
            y: "2%",
            force3D:true,
            transition: 'transform 1300ms ease',
            onComplete: () => {
                parallax([{
                    element: this.borderElement.svgBorderElement
                }], mainElement, "x,y", false, 0.002, 0.01);
            }
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.launchAnimation !== this.state.launchAnimation) {
            if (this.state.launchAnimation === true) {
                if (this.buttonElement !== null) {
                    setTimeout(() => this.setState({
                        launchButtonParralax: true
                    }, () => {
                        parallax([{
                            element: this.buttonElement
                        }], this.borderElement.svgFillElement, "top,left", true, 0.07, 0.32, true);
                    }), 350);
                    
                }
                
                this.rulesTimeline.play();
            } else {
                this.rulesTimeline.reverse();
            }
        }
    }

    onBorderEnter() {
        if (this.props.isEntered) {
            this.setState({
                launchAnimation: true
            });
        }
    }

    onBorderLeave() {
        if (this.props.isEntered) {
            this.setState({
                launchAnimation: false,
                launchButtonParralax: false
            });
        }
    }

    render () {
        return (
            <article
                className="home"
            >
                <Border
                    ref={ref => this.borderElement = ref}
                    launch={this.state.launchAnimation}
                    logo={true}
                    logoAnimation={true}
                    gradientColor='l(1, 1, 0, 0)#f05053-#e1eec3:120'
                    onMouseEnter={this.onBorderEnter.bind(this)}
                    onMouseLeave={this.onBorderLeave.bind(this)}
                />
                <div>
                    <div className="home__content">
                        <div
                            ref={ref => this.rulesElement = ref}
                            className="home__rules"
                        >
                            <div className='text text--highlight'>
                                <span></span>
                                <span>Trouve le rébus correspondant</span>
                            </div>
                            <div className='text text--highlight'>
                                <span></span>
                                <span>au titre musical proposé.</span>
                            </div>
                        </div>
                        <div className="home__actions">
                            <div
                                className={`button${ this.state.launchButtonParralax === false ? ' button--reseted' : ''}`}
                                tabIndex="0"
                                onClick={() => this.props.history.push('/game')}
                            >
                                <span ref={ref => this.buttonElement = ref} className="button__inner">Jouer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        );
    }
};

export default withRouter(Home);
"use strict";

import React from "react";
import * as ReactRedux from "react-redux";
import Response from "./response.jsx";
import Question from "./question.jsx";
import * as actions from "../../actions/item.js"

export default class Item extends React.Component {
    render () {
        let articleClassName = ["item", "game__item"];

        if (this.props.index % 2 === 0) {
            articleClassName = articleClassName.concat(["item--even"]);
        } else {
            articleClassName = articleClassName.concat(["item--odd"]);
        }

        if (this.props.success === true) {
            articleClassName = articleClassName.concat(["item--success"]);
        } else if (this.props.success === false) {
            articleClassName = articleClassName.concat(["item--fail"]);
        }

        return (
            <div id={`${"item--" + this.props.index}`} className={articleClassName.join(" ")}>
                <div className="item__background"></div>
                <section className="item__section">
                    { this.props.item !== undefined && this.props.item !== null &&
                        <Question question={this.props.item.question}/>
                    }
                </section>
                <section className="item__section">
                    { this.props.item !== undefined && this.props.item !== null &&
                        <Response
                            answer={this.props.item.answer}
                            response={this.props.response}
                            onValidate={this.props.onValidate.bind(null)}
                        />
                    }
                </section>
            </div>
        );
    }
};
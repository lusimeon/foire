"use strict";

import React from "react";
import * as ReactRedux from "react-redux";
import * as actions from "../../actions/item.js"

class Response extends React.Component {
    constructor(props) {
        super(props);
        
        this.focus = this.focus.bind(this);
        this.validationRun = false;
    }

    componentDidUpdate() {
        this.focus();
    }

    focus() {
        this.textInput.focus();
    }

    handleKeyPress(event) {
        if (event.key == "Enter") {
            event.preventDefault();

            if (this.validationRun === false) {
                this.validationRun = true;

                this.props.onValidate(() => {
                    this.validationRun = false;
                    
                    if (this.textInput !== null) {
                        this.textInput.style.height = null;
                    }
                });
            }
        }
    }

    handleChange(event) {
        // Increase height of textarea according to content
        event.currentTarget.style.height = "1px";
        event.currentTarget.style.height = event.currentTarget.scrollHeight + "px";

        this.props.setResponse(this.textInput.value);
    }

    render () {
        return (
            <div className="response item__response">
                <form className="form form--inline">
                    <textarea
                        ref={(element) => { this.textInput = element; }}
                        className="form__control form__control--text"
                        autoFocus={true}
                        value={ this.props.response }
                        onChange={ this.handleChange.bind(this) }
                        onKeyPress={ this.handleKeyPress.bind(this) }
                    ></textarea>
                </form>
            </div>
        );
    }
};

export default ReactRedux.connect(
    (state = {}) => state,
    (dispatch, props) => Object.assign({}, props, {
        "setResponse": actions.setResponse.bind(null, dispatch)
    })
)(Response);
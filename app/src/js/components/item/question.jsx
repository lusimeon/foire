"use strict";

import React from "react";
import emojify from "emojify.js";

export default class Question extends React.Component {
    componentDidMount() {
        emojify.run(this.questionElement);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.question !== prevProps.question) {
            emojify.run(this.questionElement);
        }
    }

    render () {
        return (
            <div className="question item__question">
                <span ref={ref => this.questionElement = ref} dangerouslySetInnerHTML={{"__html": this.props.question}}></span>
            </div>
        );
    }
};
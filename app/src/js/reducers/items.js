"use strict";

export default function items(state = [], action) {
    switch (action.type) {
        case "SET_ITEMS":
            state = action.items;
            break;

        default:
            return state;
    }

    return state;
}

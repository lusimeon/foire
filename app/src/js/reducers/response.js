"use strict";

export function success(state = null, action) {
    switch (action.type) {
        case "SET_SUCCESS":
            state = action.success;
            break;

        default:
            return state;
    }

    return state;
}

export function response(state = "", action) {
    switch (action.type) {
        case "SET_RESPONSE":
            state = action.response;
            break;

        default:
            return state;
    }

    return state;
}
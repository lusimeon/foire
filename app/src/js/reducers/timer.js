"use strict";

export function stage(state = "idle", action) {
    switch (action.type) {
        case "SET_STAGE":
            state = action.stage;
            break;

        default:
            return state;
    }

    return state;
}
"use strict";

export default function navigation(state = 0, action) {
    switch (action.type) {
        case "SET_CURRENT_ITEM":
            state = action.index;
            break;

        case "PREV_ITEM":
            state = state - 1;
            break;

        case "NEXT_ITEM":
            state = state + 1;
            break;

        default:
            return state;
    }

    return state;
}

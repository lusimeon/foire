"use strict";

export default function score(state = 0, action) {
    switch (action.type) {
        case "SET_SCORE":
            state = action.score;
            break;

        case "ADD_SCORE":
            state = state + action.points;
            break;

        case "SUB_SCORE":
            state = state - action.points;
            break;

        default:
            return state;
    }

    return state;
}

"use strict";

export function leaderboard(state = [], action) {
    switch (action.type) {
        case "SET_LEADERBOARD":
            state = action.leaderboard;
            break;

        default:
            return state;
    }

    return state;
}

export function isSubmitted(state = false, action) {
    switch (action.type) {
        case "SET_SUBMITTED":
            state = true;
            break;

        default:
            return state;
    }

    return state; 
}
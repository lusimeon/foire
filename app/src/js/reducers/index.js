import { combineReducers } from "redux"
import navigation from "./navigation"
import items from "./items"
import score from "./score"
import {leaderboard, isSubmitted} from "./leaderboard"
import {delay, time, stage} from "./timer"
import {success, response} from "./response"

export default combineReducers({
    navigation,
    items,
    success,
    response,
    score,
    leaderboard,
    isSubmitted,
    stage
})
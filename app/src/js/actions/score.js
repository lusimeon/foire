"use strict";

export function setScore(dispatch, score) {
    dispatch({
        "type": "SET_SCORE",
        "score": score
    });
}
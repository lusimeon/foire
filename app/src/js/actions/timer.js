"use strict";

export function setTime(dispatch, time) {
    dispatch({
        "type": "SET_TIME",
        "time": time
    });
}

export function setStage(dispatch, stage, callback) {
    dispatch({
        "type": "SET_STAGE",
        "stage": stage
    });

    if (callback !== undefined) {
    	callback();
    } 
}

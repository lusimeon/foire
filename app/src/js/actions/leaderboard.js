"use strict";

import request from "../utilities/request.js"; 

export function load(dispatch, source) {
    request(source, null, (response) => {
        dispatch({
            "type": "SET_LEADERBOARD",
            "leaderboard": response.leaderboard
        });
    });
}

export function add(dispatch, source, score, username) {
    request(source, {
        "username": username,
        "score": score
    }, (response) => {
        dispatch({
            "type": "SET_SUBMITTED"
        });

        request(source, null, (response) => {
            dispatch({
                "type": "SET_LEADERBOARD",
                "leaderboard": response.leaderboard
            });
        });
    });
}

"use strict";

import request from "../utilities/request.js"; 

export function load(dispatch, source) {
    request(source, null, (response) => {
        dispatch({
            "type": "SET_ITEMS",
            "items": response.musics
        });
    });
}

export function setResponse(dispatch, response) {
    dispatch({
        "type": "SET_RESPONSE",
        "response": response
    });
}

export function validate(dispatch, source, id, response, callback) {
    dispatch({
        "type": "SET_SUCCESS",
        "success": null
    });

    return request(source, {
        id: id,
        response: response
    }, () => {
        dispatch({
            "type": "ADD_SCORE",
            "points": 1
        });

        setTimeout(() => {
            dispatch({
                "type": "NEXT_ITEM"
            });
        }, 1000);

    }, null, (apiResponse) => {
        const success = apiResponse.success || false;

        dispatch({
            "type": "SET_SUCCESS",
            "success": success
        });

        setTimeout(() => {
            dispatch({
                "type": "SET_RESPONSE",
                "response": ""
            });

            if (callback !== undefined) {
                callback();
            }
        }, 1000);

        return apiResponse.success;
    });        

    return success;
}
"use strict";

export function setCurrentItem(dispatch, index) {
    dispatch({
        "type": "SET_CURRENT_ITEM",
        "index": index
    });
}

export function prevItem(dispatch, props) {
    dispatch({
        "type": "PREV_ITEM"
    });
}

export function nextItem(dispatch) {
    dispatch({
        "type": "NEXT_ITEM"
    });
}

"use strict";

export default function request(url, data, onSuccess = null, onFail = null, onResponse = null) {
    if (url === undefined) {
        return false;
    }

    if (data && typeof data === "object" && data.append === undefined) {
        data = JSON.stringify(data);
    }

    let xhr = new XMLHttpRequest(),
        type = data ? "POST" : "GET";

    xhr.open(type, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    try {
        xhr.onreadystatechange = function() {
            if (this.readyState === XMLHttpRequest.DONE) {
                if (this.status === 200) {
                    const response = JSON.parse(this.responseText);

                    if (response.success === true && onSuccess !== null) {
                        onSuccess(response);
                    } else if (response.success === false && onFail !== null) {
                        onFail(response);
                    }

                    if (onResponse !== null) {
                        onResponse(response);
                    }
                } else {
                    console.log("Error: %d (%s)", this.status, this.statusText);
                }
            }
        };
    } catch (e) {
        console.log("Error :", e);
    }

    xhr.send(data);

    return xhr;
};

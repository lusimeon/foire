export default (items = [], captureElement = document, type = "x,y", isPositive = true, XRatio = 1, YRatio = 1, removeEventOnLeave = false) => {
    let captureDimensions = captureElement.getBoundingClientRect();
    let itemsTrans = [];
    
    const XScale = XRatio,
        YScale = YRatio;
    
    let clientX = null,
        clientY = null;

    window.addEventListener('resize', () => {
        captureDimensions = captureElement.getBoundingClientRect();
    });

    const updateDimensions = (event) => {
        for (let itemTrans of itemsTrans) {
            const Xdifference = itemTrans.centerX - clientX;
            const Ydifference = itemTrans.centerY - clientY;

            const invertX = Xdifference > 0 ? 1 : -1;
            const invertY = Ydifference > 0 ? 1 : -1;

            let XMovement = invertX - Xdifference / captureDimensions.width;
            let YMovement = invertY - Ydifference / captureDimensions.height;
            
            XMovement = isPositive ? itemTrans.width * -XMovement : itemTrans.width * XMovement;
            YMovement = isPositive ? itemTrans.height * -YMovement : itemTrans.height * YMovement;

            XMovement = `${XScale * XMovement.toFixed(2)}%`;
            YMovement = `${YScale * YMovement.toFixed(2)}%`;

            if (type === "x,y") {
                itemTrans.element.style.transform = `translate(${XMovement}, ${YMovement})`;
            } else if (type === "top,left") {
                itemTrans.element.style.top = YMovement;
                itemTrans.element.style.left = XMovement;
            }
        }
    };

    const listenerFunction = function(event) {
        clientX = event.clientX.toFixed(2);
        clientY = event.clientY.toFixed(2);

        window.requestAnimationFrame(updateDimensions)
    };

    const removeMouseMoveEvent = () => {
        captureElement.removeEventListener('mousemove', listenerFunction, false);
        captureElement.removeEventListener('mouseleave', removeMouseMoveEvent, false);
    }

    for (let item of items) {
        let boundingClientRect = item.element.getBoundingClientRect();
        let itemTmp = {
            element: item.element,
            width: boundingClientRect.width,
            height: boundingClientRect.height
        };

        itemTmp.centerX = boundingClientRect.left + (itemTmp.width / 2);
        itemTmp.centerY = boundingClientRect.top + (itemTmp.height / 2);

        itemsTrans.push(itemTmp);
    }

    captureElement.addEventListener('mousemove', listenerFunction, false);

    if (removeEventOnLeave === true) {
        captureElement.addEventListener('mouseleave', removeMouseMoveEvent, false);
    }
};
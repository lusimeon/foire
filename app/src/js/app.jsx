"use strict";

import React from "react";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {render} from "react-dom";
import reducer from "./reducers";
import { BrowserRouter as Router, Route, Switch, withRouter, Redirect } from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Game from "./components/game.jsx";
import Result from "./components/result.jsx";
import Home from "./components/home.jsx";
import {TweenLite, TimelineLite} from "gsap";

const store = createStore(reducer);

const pageTransitionDuration = 1.75;
const PageTransition = function (props) {
    return <CSSTransition
        {...props}
        classNames="page-transition page-transition"
        timeout={pageTransitionDuration * 1000}
    >
        <div>{ props.children }</div>
    </CSSTransition>
};

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstPage: true,
            entered: false
        };
    }

    componentDidMount() {
        if (this.state.firstPage === true) {
            this.setState({
                firstPage: false,
                entered: true
            }, () => TweenLite.fromTo("article", pageTransitionDuration,{
                    opacity: 0,
                }, {
                    opacity: 1,
                    ease: Power2.easeInOut
                })
            );
        }
    }

    render() {
        return <Provider store={store}>
            <Router>
                <Route render={({ location }) => {
                    if (this.state.firstPage === true && location.pathname !== "/") {
                        return <Redirect to="/"/>
                    }
                    
                    return (
                        <TransitionGroup>
                            <PageTransition
                                in={true}
                                key={location.pathname}
                                onEnter={(element) => {
                                    TweenLite
                                        .fromTo(element, pageTransitionDuration, {
                                            xPercent: 100
                                        },
                                        {
                                            xPercent: 0,
                                            force3D: true,
                                            ease: Power2.easeInOut
                                        });
                                }}
                                onEntered={() => this.setState({
                                    entered: true
                                })}
                                onExit={(element) => {
                                    this.setState({
                                        entered: false
                                    }, () => {
                                        TweenLite
                                            .fromTo(element, pageTransitionDuration, {
                                                xPercent: 0,
                                            }, {
                                                xPercent: -100,
                                                force3D: true,
                                                ease: Power2.easeInOut
                                            });
                                    });
                                }}
                            >
                                <Switch key={location.key} location={location}>
                                    <Route exact path="/" render={() => <Home isEntered={this.state.entered}/> } />
                                    <Route exact path="/game" render={() => <Game source={process.env.API_URL + "/music"} validateSource={process.env.API_URL + "/music/validate"} isEntered={this.state.entered}/> } />
                                    <Route exact path="/result" render={() => <Result isEntered={this.state.entered}/> } />
                                </Switch>
                            </PageTransition>
                        </TransitionGroup>
                    );
                }} />
            </Router>
        </Provider>;
    }
}
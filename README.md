# FOIRE 🎉🌝
_Foire, des emojis et des sons_ ([foire.simeonlucas.fr](http://foire.simeonlucas.fr))

## Description
Trouver le titre musical correspondant au rébus proposé

## Requirements
- Node.js
- MongoDB

### Overview of technologies
- ReactJS
- ExpressJS
- GSAP
- Snap.svg
- Webpack
- Mongoose

## Install
### Launch Node server
```bash
$ cd api/
$ node index.js
````

### Build app
```bash
$ cd app/
$ npm install
$ npm run [dev|prod]
```

### Docker
```bash
$ docker-compose run --rm build npm install
$ docker-compose up -d
```

## Misc
- [Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet/)
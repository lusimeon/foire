const mongoose = require("mongoose"),
    db = mongoose.connection,
    express = require("express"),
    bodyParser = require("body-parser"),
    port = process.env.PORT || 8080,
    app = express();

mongoose.connect("mongodb://db/foire");

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("mongo connected");
});

app.disable("x-powered-by");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", process.env.SITE_URL);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(require("./controllers"));

app.listen(port);

const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    MusicSchema = new Schema({
        question: String,
        answer: String
    });

module.exports = mongoose.model("Music", MusicSchema);
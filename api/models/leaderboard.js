const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    LeaderboardSchema = new Schema({
        username: String,
        score: Number
    });

module.exports = mongoose.model("Leaderboard", LeaderboardSchema);
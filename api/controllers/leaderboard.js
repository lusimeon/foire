const Music = require("../models/music"),
    Leaderboard = require("../models/leaderboard"),
    express = require("express"),
    router = express.Router();

router.get("/", (req, res) => {
    Leaderboard.find({}).sort("-score").exec((err, leaderboards) => {
        if (err)
            res.send(err);
       
        res.json({
            "success": true,
            "leaderboard": leaderboards
        });
    });
});

router.post("/", (req, res) => {
    if (req.body.username !== undefined && req.body.score !== undefined) {
        const leaderboard = new Leaderboard({
            username: req.body.username,
            score: req.body.score
        });

        leaderboard.save((err) => {
            if (err)
                res.send(err);

            res.json({
                "success": true,
                "leaderboard": leaderboard
            });
        });
    } else {
        res.json({
            "success": false,
            "message": "Wrong parameters"
        }); 
    }
});

module.exports = router;
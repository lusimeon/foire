const Music = require("../models/music"),
    express = require("express"),
    router = express.Router();

router.get("/:musicId", (req, res) => {
    Music.findById(req.params.musicId, 'question', (err, music) => {
        if (err)
            return res.send(err);
        
        return res.json({
            "success": true,
            "music": music
        });
    });
});

router.post("/validate", (req, res) => {
    if ((req.body.id === undefined || req.body.id === NaN) || req.body.response === undefined) {
        return res.json({
            "success": false,
            "message": "Wrong parameters"
        }); 
    }

    Music.findById(req.body.id, (err, music) => {
        if (err)
            res.send(err);

        if (music === null) {
            return res.json({
                "success": false,
                "message": "Music does not exist"
            });
        }        

        const responseRegexp = new RegExp("^" + music.answer + "$", "i");

        if (responseRegexp.test(req.body.response) === false) {
            return res.json({
                "success": false,
                "message": "Wrong response"
            });
        }

        return res.json({
            "success": true,
            "music": music
        });
    });
});

router.get("/", (req, res) => {
    Music.find({}, 'question', (err, musics) => {
        if (err)
            return res.send(err);
       
        return res.json({
            "success": true,
            "musics": musics
        });
    });
});

router.post("/", (req, res) => {
    if (req.body.question === undefined || req.body.answer === undefined) {
        return res.json({
            "success": false,
            "message": "Wrong parameters"
        }); 
    }
 
    const music = new Music({
        question: req.body.question,
        answer: req.body.answer
    });
    
    music.save((err) => {
        if (err)
            return res.send(err);

        return res.json({
            "success": true,
            "music": music
        });
    });
});

module.exports = router;
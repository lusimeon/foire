const express = require("express"),
    router = express.Router();

router.use("/music", require("./music.js"));
router.use("/leaderboard", require("./leaderboard.js"));

module.exports = router;